#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
mu, sigma = 0, 25
x = mu + sigma * np.random.randn(10000)

# the histogram of the data
n, bins, patches = plt.hist(x, 50, density=1, facecolor='green', alpha=0.75)


plt.xlabel('x/y axis')
plt.ylabel('Entries')
plt.title('Histogram of IQ')
#plt.text(60, .025, r'$\mu='mu'',\ \sigma='sigma'$')
plt.axis([-100, 100, 0,0.02]) # plot range -100 to 100
plt.grid(True)
plt.show()

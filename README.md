# Excercises for TU Wien LVA 141.A89
This are some files intended to support the course UE 141.A89 at TU Wien:



## Files in this repository
- *README.md*: this file uses Markdown syntax and a cheatsheet for its syntax is available at [Github](https://guides.github.com/pdfs/markdown-cheatsheet-online.pdf)
- *lorem.txt*: Text file with _Lorem Ipsum_ fill text used for some shell command examples in this exercise
- *hallo.py*: Python "Hello World" Example
- *matplotlibtest.py*: more fancy python example creating a histogram. needs matplotlib and numpy
- *shellscipt-short.sh*
- *shellscript-long.sh*
- *rootScript.C*: example root script. start with _root rootScript.C_

---
## Exercise 1:
- Open a Terminal on your Linux/Mac
- Create workspace by cloning this git repository: git clone https://gitlab.com/dd-hephy/gittest
It will create a directory gittest. Change to this directory.
- Look into file lorem.txt. Search for the word “Wien”. 
How often does it occur and on which line number? => Protocol (Answer 1a)
- Use a shell command to count lines, words and characters in the file lorem.txt
How many lines and words does the file has? -> Protocol (Answer 1b)
- Use sed to replace each blank with a newline character. How many lines do you have now in the file?
- Use a combination of three commands using pipes to count how often each word in the file occurs 
List which 3 words occur most often and how often? => Protocol (Answer 1c)

## Excercise 2:
- Create a shell script which reads either 
  - the temperature of your CPU (**sensors** command of *lmsensors* package) or 
  - load (**w** or **uptime** command) or 
  - processor speed(s) via **cat /proc/cpuinfo**
and prints it to the console => exercise 2a

- Extend the script to 
  - Run continuously within a loop every 10 seconds => exercise 2b
  - Save the temperature, load or processor speed from exercise 2a together with a timestamp periodically to a file => exercise 2c
  - Run this program in the background => exercise 2d

Optional exercise: 
- create a listening TCP socket which returns the load or CPU temperature from Exercise 2a upon connect => exercise 2e

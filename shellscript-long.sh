#!/bin/bash

valid=true
count=1
while [ $valid ]
 do
    TIMESTAMP=`curl -s http://timestamp.web.cern.ch`
    echo $TIMESTAMP
    year=${TIMESTAMP:0:4}
    month=${TIMESTAMP:4:2}
    day=${TIMESTAMP:6:2}
    hour=${TIMESTAMP:8:2}
    minute=${TIMESTAMP:10:2}
    second=${TIMESTAMP:12:2}
    echo $year
    echo $day.$month.$year at $hour:$minute:$second
    echo Iteration: $count
    if [ $count -eq 5 ];
    then
       break
    fi
  ((count++))
  sleep 10
done